package com.example.lorentzen.studyexercisereminder.model;

public class AccelSample {
    private double x;
    private double y;
    private double z;

    public AccelSample(float x, float y, float z) {
        this.x = (double) x;
        this.y = (double) y;
        this.z = (double) z;
    }

    public double getEuclideanNorm(){
        double xPow = Math.pow(x,2);
        double yPow = Math.pow(y,2);
        double zPow = Math.pow(z,2);

        return Math.sqrt(xPow + yPow + zPow);
    }

    public double getAbsX() {
        return Math.abs(x);
    }

    public double getAbsY() {
        return Math.abs(y);
    }

    public double getAbsZ() {
        return Math.abs(z);
    }
}
