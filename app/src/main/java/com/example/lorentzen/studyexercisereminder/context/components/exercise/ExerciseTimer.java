package com.example.lorentzen.studyexercisereminder.context.components.exercise;

import android.os.CountDownTimer;

public class ExerciseTimer {
    private static ExerciseTimer timerInstance = new ExerciseTimer();
    private ExerciseTimerListener timerListener;
    private boolean isRunning = false;

    public static ExerciseTimer getInstance() {
        return timerInstance;
    }

    public void setExerciseTimerListener(ExerciseTimerListener listener) {
        this.timerListener = listener;
    }

    public void startTimeToNextExerciseTimer(double timeBetweenExercises) {
        long timeInMillis = (long) (timeBetweenExercises * 60 * 1000);
        isRunning = true;
        new CountDownTimer(timeInMillis, 1000) {
            @Override
            public void onTick(long millisUntilFinished) {
                timerListener.onTimerChanged(millisUntilFinished);
            }

            @Override
            public void onFinish() {
                timerListener.onTimerFinished();
                isRunning = false;
            }
        }.start();
    }

    public boolean isTimerRunning(){
        return isRunning;
    }

    private ExerciseTimer() {
    }

    public interface ExerciseTimerListener {
        void onTimerChanged(long millis);
        void onTimerFinished();
    }

}
