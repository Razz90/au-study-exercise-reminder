package com.example.lorentzen.studyexercisereminder.activities;

import android.app.KeyguardManager;
import android.content.Context;
import android.content.Intent;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.CountDownTimer;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.example.lorentzen.studyexercisereminder.R;
import com.example.lorentzen.studyexercisereminder.context.components.exercise.ExerciseManager;
import com.example.lorentzen.studyexercisereminder.context.components.light.LightManager;

public class ExerciseActivity extends AppCompatActivity implements ExerciseManager.ExerciseChangedListener, LightManager.LightChangedListener {

    private Button startBtn;
    private TextView currentExerciseView;
    private ExerciseManager exerciseManager;
    private LightManager lightManager;
    private KeyguardManager km;
    private boolean hasNotified = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_exercise);
        currentExerciseView = (TextView) findViewById(R.id.currentExerciseView);
        km = (KeyguardManager) getSystemService(Context.KEYGUARD_SERVICE);

        exerciseManager = new ExerciseManager(this);
        startBtn = (Button) findViewById(R.id.startBtn);

        startBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startBtn.setVisibility(View.INVISIBLE);
                lightManager = new LightManager(ExerciseActivity.this);
                exerciseManager.startTimer();
            }
        });
    }

    private void exerciseFinishedBroadcast() {
        Intent exerciseFinishedIntent = new Intent();
        exerciseFinishedIntent.setAction("exerciseFinishedEvent");

        LocalBroadcastManager.getInstance(getApplicationContext())
                .sendBroadcast(exerciseFinishedIntent);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (lightManager != null) {
            lightManager.unregisterLightListener();
        }
        exerciseManager.stopListening();
        exerciseFinishedBroadcast();
    }

    @Override
    public void onExerciseFinished() {
        Uri notification = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        final Ringtone ringtone = RingtoneManager.getRingtone(getApplicationContext(), notification);

        //start notification sound and play for 3 seconds
        ringtone.play();
        new CountDownTimer(3000, 3000) {

            @Override
            public void onTick(long millisUntilFinished) {
                //do nothing
            }

            @Override
            public void onFinish() {
                //stop notification sound and finish activity
                ringtone.stop();
                ExerciseActivity.this.finish();
            }
        }.start();
    }

    @Override
    public void onExerciseGenerated(String exercise) {
        currentExerciseView.setText(exercise);
    }

    @Override
    public void onOutOfPocket() {
        if (!hasNotified && !km.inKeyguardRestrictedInputMode()) {
            Toast.makeText(this, "Please put the phone in your pocket and perform the exercise", Toast.LENGTH_LONG).show();
            hasNotified = true;
        }
    }

    @Override
    public void onInPocket() {
        hasNotified = false;
    }
}
