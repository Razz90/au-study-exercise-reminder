package com.example.lorentzen.studyexercisereminder.activities;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.example.lorentzen.studyexercisereminder.R;
import com.example.lorentzen.studyexercisereminder.context.components.schedule.ScheduleDataService;

public class LoginActivity extends AppCompatActivity {

    private ProgressBar loginProgressBar;
    private SharedPreferences sharedPreferences;
    private String inputValue;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        sharedPreferences = getSharedPreferences(getString(R.string.preferences), MODE_PRIVATE);

        //Retrieve UI components
        loginProgressBar = (ProgressBar) findViewById(R.id.loginProgressBar);
        final Button loginButton = (Button) findViewById(R.id.loginButton);
        final EditText editTextStudyNumber = (EditText) findViewById(R.id.editTextStudyID);

        LocalBroadcastManager.getInstance(getApplicationContext()).registerReceiver(scheduleReceiver,
                new IntentFilter("scheduleResponseEvent"));

        loginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                inputValue = editTextStudyNumber.getText().toString();

                if (inputValue.matches("")) {
                    Toast.makeText(getApplicationContext(),
                            "Please enter a study ID",
                            Toast.LENGTH_SHORT).show();
                } else {
                    Intent scheduleIntent = new Intent(getApplicationContext(), ScheduleDataService.class);
                    scheduleIntent.putExtra("studyNumber", inputValue);
                    startService(scheduleIntent);
                    loginProgressBar.setVisibility(View.VISIBLE);
                }
            }
        });
    }

    //create BroadcastReceiver that receives schedule data from ScheduleDataService
    private BroadcastReceiver scheduleReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            boolean isSuccess = intent.getBooleanExtra("success", false);

            if (isSuccess) {
                loginProgressBar.setVisibility(View.INVISIBLE);

                //Stop listening
                LocalBroadcastManager.getInstance(context).unregisterReceiver(scheduleReceiver);

                //Add study ID to SharedPreferences
                SharedPreferences.Editor editor = sharedPreferences.edit();
                editor.putString(getString(R.string.study_ID), inputValue);
                editor.apply();

                //Start MainActivity again
                Intent resultIntent = new Intent(getApplicationContext(), MainActivity.class);
                startActivity(resultIntent);
                finish();
            } else {
                loginProgressBar.setVisibility(View.INVISIBLE);
                Toast.makeText(getApplicationContext(), "Failed to login. Check ID", Toast.LENGTH_SHORT).show();
            }
        }
    };

}
