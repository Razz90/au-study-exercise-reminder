package com.example.lorentzen.studyexercisereminder.activities;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.media.RingtoneManager;
import android.net.Uri;
import android.support.v4.app.TaskStackBuilder;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;
import android.widget.TextView;

import com.example.lorentzen.studyexercisereminder.R;
import com.example.lorentzen.studyexercisereminder.context.components.exercise.ExerciseTimer;
import com.example.lorentzen.studyexercisereminder.context.components.schedule.ScheduleManager;

public class MainActivity extends AppCompatActivity implements ExerciseTimer.ExerciseTimerListener {

    private ScheduleManager scheduleManager;

    private static final double TIME_BETWEEN_EXERCISES = 0.5; //time in minutes
    private boolean isInApp;

    private TextView nextExerciseTimer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //Retrieve the study ID
        SharedPreferences sharedPreferences = getSharedPreferences(getString(R.string.preferences), MODE_PRIVATE);
        String studyId = sharedPreferences.getString(getString(R.string.study_ID), null);

        if (studyId != null) {
            isInApp = true;
            nextExerciseTimer = (TextView) findViewById(R.id.nextExerciseTimer);
            scheduleManager = new ScheduleManager(this, studyId);

            ExerciseTimer.getInstance().setExerciseTimerListener(this);
            if (!ExerciseTimer.getInstance().isTimerRunning()) {
                ExerciseTimer.getInstance().startTimeToNextExerciseTimer(TIME_BETWEEN_EXERCISES);
            }

        } else {
            Intent loginIntent = new Intent(this, LoginActivity.class);
            startActivity(loginIntent);
            finish();
        }
    }

    private void sendNotification() {
        NotificationCompat.Builder nBuilder = new NotificationCompat.Builder(this)
                .setSmallIcon(R.mipmap.study_exercise_reminder)
                .setContentTitle("Study Exercise Reminder")
                .setContentText("It is time to get some exercise!")
                .setAutoCancel(true);

        Intent resIntent = new Intent(this, ExerciseActivity.class);

        TaskStackBuilder stackBuilder = TaskStackBuilder.create(this);
        stackBuilder.addParentStack(ExerciseActivity.class);
        stackBuilder.addNextIntent(resIntent);

        PendingIntent pendingIntent = stackBuilder.getPendingIntent(0, PendingIntent.FLAG_UPDATE_CURRENT);
        nBuilder.setContentIntent(pendingIntent);
        long[] vibratePattern = {500, 500, 500, 500, 500, 500};
        nBuilder.setVibrate(vibratePattern);
        nBuilder.setLights(Color.YELLOW, 500, 2000);
        Uri sound = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        nBuilder.setSound(sound);

        NotificationManager notificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
        //set notification id which allows us to update the notification later on
        notificationManager.notify(1, nBuilder.build()); //
    }

    private BroadcastReceiver exerciseFinishedReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (!ExerciseTimer.getInstance().isTimerRunning()) {
                ExerciseTimer.getInstance().startTimeToNextExerciseTimer(TIME_BETWEEN_EXERCISES);
            }
        }
    };

    @Override
    public void onTimerChanged(long millis) {
        long minutes = millis/60000;
        long seconds = (millis/1000) % 60;
        String stringSeconds = String.format("%02d", seconds);
        String time = minutes + ":" + stringSeconds;
        nextExerciseTimer.setText(time);
    }

    @Override
    public void onTimerFinished() {
        String timerFinished = "0:00";
        nextExerciseTimer.setText(timerFinished);
        if (!scheduleManager.hasLecture()) {
            if (isInApp) {
                Intent exerciseIntent = new Intent(getApplicationContext(), ExerciseActivity.class);
                startActivity(exerciseIntent);
            } else {
                //send notification to the user
                sendNotification();
            }
        } else {
            ExerciseTimer.getInstance().startTimeToNextExerciseTimer(TIME_BETWEEN_EXERCISES);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        scheduleManager.registerReceiver();
        LocalBroadcastManager.getInstance(this).registerReceiver(exerciseFinishedReceiver,
                new IntentFilter("exerciseFinishedEvent"));
        isInApp = true;
    }

    @Override
    protected void onPause() {
        super.onPause();
        scheduleManager.unregisterReceiver();
    }

    @Override
    protected void onStop() {
        super.onStop();
        isInApp = false;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        LocalBroadcastManager.getInstance(this).unregisterReceiver(exerciseFinishedReceiver);
    }
}