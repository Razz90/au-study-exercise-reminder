package com.example.lorentzen.studyexercisereminder.context.components.exercise;

import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.CountDownTimer;
import android.os.PowerManager;
import android.os.Vibrator;

import com.example.lorentzen.studyexercisereminder.model.AccelDataSet;
import com.example.lorentzen.studyexercisereminder.model.AccelSample;
import com.example.lorentzen.studyexercisereminder.model.ExerciseType;

import java.util.ArrayList;

public class ExerciseManager implements SensorEventListener {

    private static final int ACCELDATASET_SIZE = 128;
    private static final double EXERCISE_TIMEOUT = 5;
    private String currentExerciseToPerform;

    private ArrayList<AccelSample> accelSampleArrayList = new ArrayList<>();
    private ArrayList<AccelDataSet> accelDataSetArrayList = new ArrayList<>();

    private SensorManager sensorManager;
    private Vibrator vibrator;
    private PowerManager.WakeLock wakeLock;
    private ExerciseInterpreter exerciseInterpreter;
    private ExerciseChangedListener exerciseChangedListener;
    private CountDownTimer countDownTimer;

    public ExerciseManager(Context context) {
        sensorManager = (SensorManager) context.getSystemService(Context.SENSOR_SERVICE);
        vibrator = (Vibrator) context.getSystemService(Context.VIBRATOR_SERVICE);
        PowerManager powerManager = (PowerManager) context.getSystemService(Context.POWER_SERVICE);
        wakeLock = powerManager.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK, "WakeLock");
        exerciseInterpreter = new ExerciseInterpreter(context);

        if (context instanceof ExerciseChangedListener){
            exerciseChangedListener = (ExerciseChangedListener) context;
        }

        //Choose a random exercise to perform
        generateRandomExercise();
    }

    public String getCurrentExercisePerformed() {
        String currentExercise = exerciseInterpreter.classifyData(accelDataSetArrayList);
        clearLists();

        return currentExercise;
    }

    public void startListening() {
        wakeLock.acquire();
        sensorManager.registerListener(this, sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER), SensorManager.SENSOR_DELAY_UI);
    }

    public void stopListening() {
        //Release wakelock, unregister accelerometer, stop timer and clear lists
        if (wakeLock.isHeld()) {
            wakeLock.release();
        }
        if (countDownTimer != null) {
            countDownTimer.cancel();
        }
        sensorManager.unregisterListener(this);
        clearLists();
    }

    @Override
    public void onSensorChanged(SensorEvent event) {
        if(event.sensor.getType() == Sensor.TYPE_ACCELEROMETER){
            float[] sensorValues = event.values;
            float x = sensorValues[0];
            float y = sensorValues[1];
            float z = sensorValues[2];

            accelSampleArrayList.add(new AccelSample(x, y, z));

            if (accelSampleArrayList.size() == ACCELDATASET_SIZE) {
                AccelDataSet dataSet = new AccelDataSet(accelSampleArrayList);
                accelDataSetArrayList.add(dataSet);

                //Remove the first entries from the ArrayList in order to get overlapping windows
                for (int i = 0; i < ACCELDATASET_SIZE/2; i++) {
                    accelSampleArrayList.remove(0);
                }
            }
        }
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {
        //Do nothing
    }

    private void clearLists() {
        accelSampleArrayList.clear();
        accelDataSetArrayList.clear();
    }

    public void startTimer() {
        //Start listening for accelerometer data and start the timer
        startListening();

        long timeInMillis = (long) (EXERCISE_TIMEOUT * 60 * 1000);
        countDownTimer = new CountDownTimer(timeInMillis, 10000) {
            boolean firstTime = true;
            String mode;
            int timeExercisePerformed = 0;
            @Override
            public void onTick(long millisUntilFinished) {
                if (!firstTime) {
                    mode = getCurrentExercisePerformed();
                    if (mode.equals(currentExerciseToPerform)){
                        timeExercisePerformed += 10;
                        vibrator.vibrate(2000);
                    }
                } else {
                    firstTime = false;
                }

                if (timeExercisePerformed == 30) {
                    stopListening();
                    exerciseChangedListener.onExerciseFinished();
                }
            }

            @Override
            public void onFinish() {
                stopListening();
                exerciseChangedListener.onExerciseFinished();
            }
        }.start();
    }

    private void generateRandomExercise() {
        int exerciseInt = (int) (Math.random() * 3 + 1);
        switch (exerciseInt) {
            case 1:
                currentExerciseToPerform = ExerciseType.JUMPING_JACK;
                break;
            case 2:
                currentExerciseToPerform = ExerciseType.KNEE_LIFT;
                break;
            case 3:
                currentExerciseToPerform = ExerciseType.WALKING_LUNGES;
                break;
        }
        exerciseChangedListener.onExerciseGenerated(currentExerciseToPerform);
    }

    public interface ExerciseChangedListener {
        void onExerciseFinished();
        void onExerciseGenerated(String exercise);
    }
}
