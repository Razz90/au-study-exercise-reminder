package com.example.lorentzen.studyexercisereminder.context.components.exercise;

import android.content.Context;
import android.content.res.AssetManager;
import android.util.Log;

import com.example.lorentzen.studyexercisereminder.model.AccelDataSet;
import com.example.lorentzen.studyexercisereminder.model.ExerciseType;

import java.io.ObjectInputStream;
import java.util.ArrayList;
import java.util.HashMap;

import weka.classifiers.trees.J48;
import weka.core.Attribute;
import weka.core.FastVector;
import weka.core.Instance;
import weka.core.Instances;

public class ExerciseInterpreter {

    private static final double exerciseTypePlaceholder = 1337.0;

    private J48 J48Tree;
    private Instances instances;

    public ExerciseInterpreter(Context context) {
        AssetManager assetManager = context.getAssets();
        initializeModel(assetManager);
    }

    private FastVector setupAttributes() {
        FastVector attributes = new FastVector();
        attributes.addElement(new Attribute("XMinMag"));
        attributes.addElement(new Attribute("YMinMag"));
        attributes.addElement(new Attribute("ZMinMag"));
        attributes.addElement(new Attribute("XMaxMag"));
        attributes.addElement(new Attribute("YMaxMag"));
        attributes.addElement(new Attribute("ZMaxMag"));
        attributes.addElement(new Attribute("XStd"));
        attributes.addElement(new Attribute("YStd"));
        attributes.addElement(new Attribute("ZStd"));

        FastVector modes = new FastVector();
        modes.addElement(ExerciseType.KNEE_LIFT);
        modes.addElement(ExerciseType.JUMPING_JACK);
        modes.addElement(ExerciseType.WALKING_LUNGES);
        modes.addElement(ExerciseType.STANDING_STILL);

        attributes.addElement(new Attribute("mode", modes));

        return attributes;
    }

    private void initializeModel(AssetManager assetManager) {
        try {
            J48Tree = (J48) new ObjectInputStream(assetManager.open("ExerciseClassifier.model")).readObject();
            instances = new Instances("movementResults", setupAttributes(), 0);
            instances.setClassIndex(instances.numAttributes() - 1);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void populateInstances(ArrayList<AccelDataSet> accelDataSets) {
        for (AccelDataSet dataSet : accelDataSets) {
            instances.add(createInstanceFromAccelDataSet(dataSet));
        }
    }

    private Instance createInstanceFromAccelDataSet(AccelDataSet dataSet) {
        double[] values = new double[10];
        values[0] = dataSet.getXMinMagValue();
        values[1] = dataSet.getYMinMagValue();
        values[2] = dataSet.getZMinMagValue();
        values[3] = dataSet.getXMaxMagValue();
        values[4] = dataSet.getYMaxMagValue();
        values[5] = dataSet.getZMaxMagValue();
        values[6] = dataSet.getXStd();
        values[7] = dataSet.getYStd();
        values[8] = dataSet.getZStd();
        values[9] = exerciseTypePlaceholder; // Placeholder value
        return new Instance(1.0, values);
    }

    public String classifyData(ArrayList<AccelDataSet> arrayListDataSet){
        populateInstances(arrayListDataSet);
        Instances labeled = new Instances(instances);
        String exerciseResult = null;

        //loop instances
        for (int i = 0; i < instances.numInstances(); i++) {
            try {
                double classLabel = J48Tree.classifyInstance(instances.instance(i));
                labeled.instance(i).setClassValue(classLabel);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        //clear the instances after labelling them
        instances.delete();

        //setting up a hashmap to find the exercise with highest occurrence
        HashMap<String, Integer> exerciseHashMap = new HashMap<>();
        exerciseHashMap.put(ExerciseType.KNEE_LIFT, 0);
        exerciseHashMap.put(ExerciseType.JUMPING_JACK, 0);
        exerciseHashMap.put(ExerciseType.WALKING_LUNGES, 0);
        exerciseHashMap.put(ExerciseType.STANDING_STILL, 0);

        for (int i = 0; i < labeled.numInstances(); i++) {
            String mode = labeled.instance(i).stringValue(labeled.numAttributes() - 1); //check mode

            switch (mode) {
                case ExerciseType.KNEE_LIFT:
                    exerciseHashMap.put(ExerciseType.KNEE_LIFT,
                            exerciseHashMap.get(ExerciseType.KNEE_LIFT) + 1);
                    break;
                case ExerciseType.JUMPING_JACK:
                    exerciseHashMap.put(ExerciseType.JUMPING_JACK,
                            exerciseHashMap.get(ExerciseType.JUMPING_JACK) + 1);
                    break;
                case ExerciseType.WALKING_LUNGES:
                    exerciseHashMap.put(ExerciseType.WALKING_LUNGES,
                            exerciseHashMap.get(ExerciseType.WALKING_LUNGES) + 1);
                    break;
                case ExerciseType.STANDING_STILL:
                    exerciseHashMap.put(ExerciseType.STANDING_STILL,
                            exerciseHashMap.get(ExerciseType.STANDING_STILL) + 1);
                    break;
            }
        }

        int maxValue = 0;

        for (String exerciseType : exerciseHashMap.keySet()) {
            int value =  exerciseHashMap.get(exerciseType);

            if(value > maxValue) {
                maxValue = value;
                exerciseResult = exerciseType;
            }
        }

        //logging guesses for each data set
        Log.d("STUDY EXERCISE REMINDER", ExerciseType.KNEE_LIFT + ": " + exerciseHashMap.get(ExerciseType.KNEE_LIFT));
        Log.d("STUDY EXERCISE REMINDER", ExerciseType.WALKING_LUNGES + ": " + exerciseHashMap.get(ExerciseType.WALKING_LUNGES));
        Log.d("STUDY EXERCISE REMINDER", ExerciseType.JUMPING_JACK + ": " + exerciseHashMap.get(ExerciseType.JUMPING_JACK));
        Log.d("STUDY EXERCISE REMINDER", ExerciseType.STANDING_STILL + ": " + exerciseHashMap.get(ExerciseType.STANDING_STILL));

        return exerciseResult;
    }
}
