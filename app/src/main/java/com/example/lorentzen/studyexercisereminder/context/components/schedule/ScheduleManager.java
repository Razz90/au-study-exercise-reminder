package com.example.lorentzen.studyexercisereminder.context.components.schedule;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.support.v4.content.LocalBroadcastManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Locale;

public class ScheduleManager {
    private JSONObject scheduleData;
    private Context context;
    private String studyNumber;

    public ScheduleManager(Context context, String studyNumber) {
        this.context = context;
        this.studyNumber = studyNumber;
        startScheduleDataService();
    }

    public boolean hasLecture() {

        if (scheduleData == null) {
            return false;
        }

        try {
            JSONArray coursesArray = scheduleData.getJSONArray("courses");

            Date date = Calendar.getInstance().getTime();
            String currentDay = new SimpleDateFormat("EEEE", new Locale("da", "DK")).format(date);
            int currentWeek = new GregorianCalendar().get(Calendar.WEEK_OF_YEAR);
            int currentTime = Integer.parseInt(new SimpleDateFormat("HH", Locale.ENGLISH).format(date));

            // Checking if a given course is in the current week
            for (int i = 0; i < coursesArray.length(); i++) {
                boolean isWeekTheSame = false;
                boolean isDayTheSame = false;
                boolean isTimeTheSame = false;

                JSONObject course = coursesArray.getJSONObject(i);
                String week = course.getString("week");
                String day = course.getString("day");
                String time = course.getString("time");

                String[] weekArray = week.split(",");
                for (String weekEntity : weekArray) {

                    if(!weekEntity.contains("-") && Integer.parseInt(weekEntity.trim()) == currentWeek) {
                        isWeekTheSame = true;
                    } else if (weekEntity.contains("-")) {
                        String[] weekInterval = weekEntity.split("-");
                        int intervalStart = Integer.parseInt(weekInterval[0].trim());
                        int intervalEnd = Integer.parseInt(weekInterval[1].trim());
                        if (currentWeek >= intervalStart && currentWeek <= intervalEnd) {
                            isWeekTheSame = true;
                        }
                    }
                }

                if(day.equals(currentDay)) {
                    isDayTheSame = true;
                }

                String[] timeInterval = time.split("-");
                int intervalStart = Integer.parseInt(timeInterval[0].trim());
                int intervalEnd = Integer.parseInt(timeInterval[1].trim());
                if(currentTime >= intervalStart && currentTime < intervalEnd) {
                    isTimeTheSame = true;
                }

                if (isWeekTheSame && isDayTheSame && isTimeTheSame) {
                    return true;
                }

            }

            return false;

        } catch (JSONException e) {
            e.printStackTrace();
            return false;
        }
    }

    //create BroadcastReceiver that receives schedule data from ScheduleDataService
    private BroadcastReceiver scheduleReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String scheduleJsonString = intent.getStringExtra("scheduleData");
            try {
                scheduleData = new JSONObject(scheduleJsonString);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    };

    public void registerReceiver() {
        LocalBroadcastManager.getInstance(context).registerReceiver(scheduleReceiver,
                new IntentFilter("scheduleResponseEvent"));
    }

    public void unregisterReceiver() {
        LocalBroadcastManager.getInstance(context).unregisterReceiver(scheduleReceiver);
    }

    private void startScheduleDataService() {
        Intent scheduleIntent = new Intent(context, ScheduleDataService.class);
        scheduleIntent.putExtra("studyNumber", studyNumber);
        context.startService(scheduleIntent);
    }
}
