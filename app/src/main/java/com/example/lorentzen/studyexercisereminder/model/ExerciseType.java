package com.example.lorentzen.studyexercisereminder.model;

public class ExerciseType {
    //these strings must be equal to the values in the attribute 'mode' in the model
    public static final String KNEE_LIFT = "Knee Lift";
    public static final String JUMPING_JACK = "Jumping Jack";
    public static final String WALKING_LUNGES = "Walking Lunges";
    public static final String STANDING_STILL = "Standing Still";
}
