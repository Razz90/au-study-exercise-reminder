package com.example.lorentzen.studyexercisereminder.context.components.schedule;

import android.app.IntentService;
import android.content.Intent;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONObject;

/**
 * An {@link IntentService} subclass for handling asynchronous task requests in
 * a service on a separate handler thread.
 * <p/>
 * TODO: Customize class - update intent actions, extra parameters and static
 * helper methods.
 */
public class ScheduleDataService extends IntentService {

    private Intent scheduleResponseIntent = new Intent();

    public ScheduleDataService() {
        super("ScheduleDataService");
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        //get schedule data from external service
        Log.d("SCHEDULE DATA SERVICE", "Getting schedule data from external service");

        RequestQueue queue = Volley.newRequestQueue(this);

        String studyNumber = intent.getStringExtra("studyNumber");
        String url = "http://skema.lasselegaard.dk/?aarskort=" + studyNumber;

        JsonObjectRequest request = new JsonObjectRequest(Request.Method.GET, url, null, new Response.Listener<JSONObject>() {

            @Override
            public void onResponse(JSONObject response) {
                scheduleResponseIntent.setAction("scheduleResponseEvent");
                scheduleResponseIntent.putExtra("scheduleData", response.toString());

                if (response.toString().contains("error")) {
                    scheduleResponseIntent.putExtra("success", false);
                } else {
                    scheduleResponseIntent.putExtra("success", true);
                }

                LocalBroadcastManager.getInstance(getApplicationContext())
                        .sendBroadcast(scheduleResponseIntent);
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d("SCHEDULE DATA", "Error retrieving the schedule data");
                scheduleResponseIntent.setAction("scheduleResponseEvent");
                scheduleResponseIntent.putExtra("success", false);
                scheduleResponseIntent.putExtra("scheduleData", "");

                LocalBroadcastManager.getInstance(getApplicationContext())
                        .sendBroadcast(scheduleResponseIntent);
            }
        });

        request.setRetryPolicy(new DefaultRetryPolicy(20000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        //add the request to the queue
        queue.add(request);
    }
}
