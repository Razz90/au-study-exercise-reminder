package com.example.lorentzen.studyexercisereminder.context.components.light;

import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Handler;

import java.util.ArrayList;

public class LightManager implements SensorEventListener {

    private LightChangedListener listener;
    private SensorManager sensorManager;
    private ArrayList<Float> lightLevelArrayList = new ArrayList<>();
    private float avgLightLevel;
    private final Sensor lightSensor;

    public LightManager(Context context) {
        sensorManager = (SensorManager) context.getSystemService(Context.SENSOR_SERVICE);
        lightSensor = sensorManager.getDefaultSensor(Sensor.TYPE_LIGHT);

        //Register lightSensor after 4 seconds delay
        Runnable registerSensorTask = new Runnable() {
            @Override
            public void run() {
                sensorManager.registerListener(LightManager.this, lightSensor, SensorManager.SENSOR_DELAY_NORMAL);
            }
        };
        Handler lightHandler = new Handler();
        lightHandler.postDelayed(registerSensorTask, 4000);

        if (context instanceof LightChangedListener) {
            listener = (LightChangedListener) context;
        }
    }

    @Override
    public void onSensorChanged(SensorEvent event) {
        float lightLevel = event.values[0];
        lightLevelArrayList.add(lightLevel);

        if (lightLevelArrayList.size() == 20) {
            for (Float lightlvl : lightLevelArrayList) {
                avgLightLevel += lightlvl;
            }
            avgLightLevel = avgLightLevel/lightLevelArrayList.size();

            if (avgLightLevel > 10) {
                listener.onOutOfPocket();
            } else {
                listener.onInPocket();
            }

            lightLevelArrayList.clear();
        }
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {

    }

    public void unregisterLightListener() {
        sensorManager.unregisterListener(this);
    }

    public interface LightChangedListener {
        void onOutOfPocket();
        void onInPocket();
    }

}
