package com.example.lorentzen.studyexercisereminder.model;

import java.util.ArrayList;

public class AccelDataSet {

    private ArrayList<Double> XArrayList = new ArrayList<>();
    private ArrayList<Double> YArrayList = new ArrayList<>();
    private ArrayList<Double> ZArrayList = new ArrayList<>();

    public AccelDataSet(ArrayList<AccelSample> sampleArray) {
        for (AccelSample sample : sampleArray) {
            XArrayList.add(sample.getAbsX());
            YArrayList.add(sample.getAbsY());
            ZArrayList.add(sample.getAbsZ());
        }
    }

    private double getStandardDeviation(ArrayList<Double> accelList){
        double sumOfSquaredNumbers = 0;

        double mean;
        double variance;
        double standardDeviation ;

        mean = getMeanValue(accelList);

        for (Double axis : accelList) {
            sumOfSquaredNumbers += Math.pow((axis - mean), 2);
        }

        variance = (sumOfSquaredNumbers / (accelList.size() - 1));

        standardDeviation = Math.sqrt(variance);

        return standardDeviation;

    }

    private double getMaxMagValue(ArrayList<Double> accelList){
        double maxValue = accelList.get(0);

        for (Double axis : accelList) {
            maxValue = axis > maxValue ? axis : maxValue;
        }

        return maxValue;
    }

    private double getMinMagValue(ArrayList<Double> accelList){
        double minValue = accelList.get(0);

        for (Double axis : accelList) {
            minValue = axis < minValue ? axis : minValue;
        }

        return minValue;
    }

    private double getMeanValue(ArrayList<Double> accelList){
        double sumOfValues = 0;
        double mean;

        for (Double axis : accelList) {
            sumOfValues += axis;
        }

        mean = sumOfValues / accelList.size();

        return mean;
    }

    public double getXMaxMagValue() {
        return getMaxMagValue(XArrayList);
    }

    public double getYMaxMagValue() {
        return getMaxMagValue(YArrayList);
    }

    public double getZMaxMagValue() {
        return getMaxMagValue(ZArrayList);
    }

    public double getXMinMagValue() {
        return getMinMagValue(XArrayList);
    }

    public double getYMinMagValue() {
        return getMinMagValue(YArrayList);
    }

    public double getZMinMagValue() {
        return getMinMagValue(ZArrayList);
    }

    public double getXStd() {
        return getStandardDeviation(XArrayList);
    }

    public double getYStd() {
        return getStandardDeviation(YArrayList);
    }

    public double getZStd() {
        return getStandardDeviation(ZArrayList);
    }

}
