# Login
The login screen uses data from an AU website (http://services.science.au.dk/apps/skema/VaelgElevskema.asp?webnavn=skema)
and therefore it expects a valid study ID. In case you do not have any, you can write _test_ instead and you will be logged in using test data.

# Time to next exercise
On this screen you will find a count down clock which indicates, when the next exercise will occur. When the clock hits '0:00' the exercise screen will pop up. In case you are not in the application, you will receive a notification instead – press the notification and you will arrive at the exercise screen.

# Perform exercise
The exercise screen will tell you what exercise to perform. Once you are ready, you can hit _Perform exercise_ and listening for accelerometer events. If you do not put the phone in your pocket, the application will ask you to do so. During the exercise, the application will vibrate every 10th second in order to let you know that you are performing the exercise correctly. Once the exercise is done, the application will play a sound to let the user know that the exercise is finished.
